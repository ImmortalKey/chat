<?php
function login(){

   if(isset($_POST["btn"])){          
       $username  = htmlentities($_POST['username']);
       $password = htmlentities($_POST['password']);
       require_once("../model/model.php");
       $model=new crud();
       $data =$model->readBy('users ',"*","username=?",[$username]);
       $data=$data->fetchAll();
        
       if($data){

            if(password_verify($password,$data[0]['pwd'])){
               
                $_SESSION['user']=$data[0]['nom'].' '.$data[0]['prenom'];
                $_SESSION['user_id']=$data[0]['id'];
                header("location:index.php?control=home");
            }else{
                $error='Mot de passe incorrect';
            }
           
       }else{
           $error="Compte non Trouvé";
       
       }

   }
    
   require('../views/login.php');
}

function register(){
    if (isset($_POST['btn'])){

        require_once("../model/model.php");
        $data=[$_POST['nom'],$_POST['prenom'],$_POST['age'],$_POST['sexe'],$_POST['username'],password_hash($_POST['password'],PASSWORD_DEFAULT)]; 
        $crud= new crud() ;
        $crud->create('users','nom,prenom,age,sexe,username,pwd','?,?,?,?,?,?',$data);
        echo "<script>alert('Inscription reussi')</script>";
        header("location:index.php?control=login");
    }
    require('../views/register.php');
}


function logout(){
    session_destroy();
    header("location:index.php?control=login");
}

